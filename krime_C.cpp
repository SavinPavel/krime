﻿#include <Windows.h>
#include <stdio.h>
#include <vector>
#include <iostream>

DWORD RVA2VA(DWORD dwRVA, DWORD dwVASection, DWORD dwRawSection) {
    return dwRVA - dwVASection + dwRawSection;
}

 
int main()
{
    HANDLE hFile = NULL;
    PVOID pvBuffer = NULL;
    DWORD dwFileSize = 0;
    DWORD dwImportVA = 0;

    hFile = CreateFile("lab3.exe", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if (hFile != INVALID_HANDLE_VALUE) {
        dwFileSize = GetFileSize(hFile, 0);
        pvBuffer = LocalAlloc(LPTR, dwFileSize);
        if (pvBuffer) {
            DWORD dwBytesRead = 0;
            if (!ReadFile(hFile, pvBuffer, dwFileSize, &dwBytesRead, NULL)) {
                LocalFree(pvBuffer);
                CloseHandle(hFile);
            }
        }
    }
    PIMAGE_DOS_HEADER pDos = (PIMAGE_DOS_HEADER)pvBuffer;
    printf("DOS_HEADER->e_magic:0%X\n", pDos->e_magic);
    printf("DOS_HEADER->e_lfarlc:0%X\n", pDos->e_lfarlc);
    printf("DOS_HEADER->e_lfanew:0%X\n", pDos->e_lfanew);
    PIMAGE_NT_HEADERS32 pNT = (PIMAGE_NT_HEADERS32)((DWORD)pvBuffer + pDos->e_lfanew);
    printf("NT_HEADERS->signature:0%X\n", pNT->Signature);
    printf("NT_HEADERS->FileHeader->Machine:0%X\n", pNT->FileHeader.Machine);
    printf("NT_HEADERS->FileHeader->NumberOfSections:0%d\n", pNT->FileHeader.NumberOfSections);
    printf("NT_HEADERS->FileHeader->TimeDateStamp:0%X\n", pNT->FileHeader.TimeDateStamp);
    printf("NT_HEADERS->FileHeader->SizeOfOptionalHeader:0%X\n", pNT->FileHeader.SizeOfOptionalHeader);
    printf("NT_HEADERS->FileHeader->Characteristics:0%X\n", pNT->FileHeader.Characteristics);
    printf("NT_HEADERS->OptionalHeader.Magic:0%X\n", pNT->OptionalHeader.Magic);
    printf("NT_HEADERS->OptionalHeader.MajorLinkerVersion:0%X\n", pNT->OptionalHeader.MajorLinkerVersion);
    printf("NT_HEADERS->OptionalHeader.MinorLinkerVersion:0%X\n", pNT->OptionalHeader.MinorLinkerVersion);
    printf("NT_HEADERS->OptionalHeader.SizeOfCode:0%X\n", pNT->OptionalHeader.SizeOfCode);
    printf("NT_HEADERS->OptionalHeader.SizeOfInitializedData:0%X\n", pNT->OptionalHeader.SizeOfInitializedData);
    printf("NT_HEADERS->OptionalHeader.SizeOfUninitializedData:0%X\n", pNT->OptionalHeader.SizeOfUninitializedData);
    printf("NT_HEADERS->OptionalHeader.AddressOfEntryPoint:0%X\n", pNT->OptionalHeader.AddressOfEntryPoint);
    printf("NT_HEADERS->OptionalHeader.BaseOfCode:0%X\n", pNT->OptionalHeader.BaseOfCode);
    printf("NT_HEADERS->OptionalHeader.BaseOfData:0%X\n", pNT->OptionalHeader.BaseOfData);
    printf("NT_HEADERS->OptionalHeader.ImageBase:0%X\n", pNT->OptionalHeader.ImageBase);
    printf("NT_HEADERS->OptionalHeader.SectionAlignment:0%X\n", pNT->OptionalHeader.SectionAlignment);
    printf("NT_HEADERS->OptionalHeader.FileAlignment:0%X\n", pNT->OptionalHeader.FileAlignment);
    printf("NT_HEADERS->OptionalHeader.MajorOperatingSystemVersion:0%X\n", pNT->OptionalHeader.MajorOperatingSystemVersion);
    printf("NT_HEADERS->OptionalHeader.MinorOperatingSystemVersion:0%X\n", pNT->OptionalHeader.MinorOperatingSystemVersion);
    printf("NT_HEADERS->OptionalHeader.SizeOfImage:0%X\n", pNT->OptionalHeader.SizeOfImage);
    printf("NT_HEADERS->OptionalHeader.SizeOfHeaders:0%X\n", pNT->OptionalHeader.SizeOfHeaders);
    printf("NT_HEADERS->OptionalHeader.CheckSum:0%X\n", pNT->OptionalHeader.CheckSum);
    printf("NT_HEADERS->OptionalHeader.Subsystem:0%X\n", pNT->OptionalHeader.Subsystem);
    printf("NT_HEADERS->OptionalHeader.DllCharacteristics:0%X\n", pNT->OptionalHeader.DllCharacteristics);
    printf("NT_HEADERS->OptionalHeader.DataDirectory:0%X\n", pNT->OptionalHeader.DataDirectory);
    
    PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION(pNT);
    dwImportVA = pNT->OptionalHeader.DataDirectory[1].VirtualAddress;
    DWORD dwImportSize = 0;
    dwImportSize= pNT->OptionalHeader.DataDirectory[1].Size;
    DWORD dwNumSecImport = 0;
    std::vector<PIMAGE_SECTION_HEADER> pSecVector;
    for (size_t i = 0; i < pNT->FileHeader.NumberOfSections ; i++){
        pSecVector.push_back(pSection);
        printf("Section Name: %s\n", (pSection->Name));
        pSection++;
    }
    
    for (size_t i = 0; i < pNT->FileHeader.NumberOfSections; i++) {
        if (pSecVector[i]->VirtualAddress > dwImportVA) {
            dwNumSecImport = i - 1;
            break;
        }
        pSection++;
    }

    DWORD dwRVAImport = RVA2VA(dwImportVA, pSecVector[dwNumSecImport]->VirtualAddress, pSecVector[dwNumSecImport]->PointerToRawData);
    PIMAGE_IMPORT_DESCRIPTOR pDesc = (PIMAGE_IMPORT_DESCRIPTOR)((DWORD)pvBuffer + dwRVAImport);

    while(pDesc->OriginalFirstThunk){
        printf("\n");
        DWORD dwVADLL = RVA2VA(pDesc->Name, pSecVector[dwNumSecImport]->VirtualAddress, pSecVector[dwNumSecImport]->PointerToRawData);
        printf("DLL Name: %s\n", ((DWORD)pvBuffer + dwVADLL));
        DWORD dwVAThunkData = RVA2VA(pDesc->OriginalFirstThunk, pSecVector[dwNumSecImport]->VirtualAddress, pSecVector[dwNumSecImport]->PointerToRawData);
        PIMAGE_THUNK_DATA pThunk = (PIMAGE_THUNK_DATA)((DWORD)pvBuffer + dwVAThunkData);

        while (pThunk->u1.AddressOfData) {
            DWORD dwVAImportNT = RVA2VA(pThunk->u1.AddressOfData, pSecVector[dwNumSecImport]->VirtualAddress, pSecVector[dwNumSecImport]->PointerToRawData);
            PIMAGE_IMPORT_BY_NAME pINT = (PIMAGE_IMPORT_BY_NAME)((DWORD)pvBuffer + dwVAImportNT);
            printf("Function Name: %s\n", (pINT->Name));
            pThunk++;
        }
        pDesc++;
    }

    system("pause");
    return 0;
 }

